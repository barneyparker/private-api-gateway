variable "vpc_id" {
  default = "vpc-a79bb4cf"
}

data "aws_subnet_ids" "subnets" {
  vpc_id = var.vpc_id
}

resource "aws_vpc_endpoint" "api_endpoint" {
  vpc_id       = var.vpc_id
  service_name = "com.amazonaws.eu-west-2.execute-api"
  vpc_endpoint_type = "Interface"
  private_dns_enabled = true
  security_group_ids = [aws_security_group.endpoint.id]
  subnet_ids = data.aws_subnet_ids.subnets.ids
}

resource "aws_security_group" "endpoint" {
  name = "gel_api"
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "endpoint_ingress_all" {
  security_group_id = aws_security_group.endpoint.id
  type = "ingress"
  description = "everything"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}