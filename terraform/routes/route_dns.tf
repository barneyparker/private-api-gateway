resource "aws_api_gateway_resource" "dns" {
  rest_api_id = var.api_id
  parent_id   = var.root_resource_id
  path_part   = "dns"
}

resource "aws_api_gateway_method" "dns" {
  rest_api_id   = var.api_id
  resource_id   = aws_api_gateway_resource.dns.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "dns" {
  rest_api_id = var.api_id
  resource_id = aws_api_gateway_resource.dns.id
  http_method = aws_api_gateway_method.dns.http_method
  integration_http_method = "POST"
  type = "AWS_PROXY"
  uri = aws_lambda_function.dns_lambda.invoke_arn
}

resource "aws_api_gateway_method_response" "dns_response_200" {
  rest_api_id = var.api_id
  resource_id = aws_api_gateway_resource.dns.id
  http_method = aws_api_gateway_method.dns.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "dns_200_response" {
  rest_api_id = var.api_id
  resource_id = aws_api_gateway_resource.dns.id
  http_method = aws_api_gateway_method.dns.http_method
  status_code = aws_api_gateway_method_response.dns_response_200.status_code

  depends_on = [aws_api_gateway_method_response.dns_response_200]
}

data "archive_file" "dns_lambda" {
  type = "zip"
  output_path = "${path.module}/.terraform/dns_lambda.zip"
  source {
    filename = "index.js"
    content = <<-EOF
      module.exports.handler = async (event, context, callback) => {
        const response = {
          statusCode: 200,
          body: JSON.stringify({
            message: 'Hello from API Gateway!',
            details: event,
          }, null, 2),
        }
        return response
      }
    EOF
  }
}

resource "aws_iam_role" "dns_lambda" {
  name = "dns_lambda_role"
  assume_role_policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "lambda.amazonaws.com"
          }
        }
      ]
    }
  EOF
}

resource "aws_lambda_function" "dns_lambda" {
  function_name = "dns_lambda"
  filename = data.archive_file.dns_lambda.output_path
  source_code_hash = data.archive_file.dns_lambda.output_base64sha256

  runtime = "nodejs12.x"
  handler = "index.handler"

  role = aws_iam_role.dns_lambda.arn
}

resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "APIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.dns_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${var.api_id}/*/${aws_api_gateway_method.dns.http_method}/dns" # TODO: should the last * be the method?
}