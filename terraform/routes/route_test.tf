#resource "aws_api_gateway_resource" "root" {
#  rest_api_id = var.api_id
#  parent_id   = var.root_resource_id
#  path_part   = "/"
#}

resource "aws_api_gateway_method" "root" {
  rest_api_id   = var.api_id
  resource_id   = var.root_resource_id # aws_api_gateway_resource.root.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "root" {
  rest_api_id = var.api_id
  resource_id = var.root_resource_id # aws_api_gateway_resource.root.id
  http_method = aws_api_gateway_method.root.http_method
  type        = "MOCK"
  request_templates = {
    "application/json" = <<-EOF
      {
        "statusCode": 200
      }
    EOF
  }
}

resource "aws_api_gateway_method_response" "root_response_200" {
  rest_api_id = var.api_id
  resource_id = var.root_resource_id # aws_api_gateway_resource.root.id
  http_method = aws_api_gateway_method.root.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "root_200_response" {
  rest_api_id = var.api_id
  resource_id = var.root_resource_id # aws_api_gateway_resource.root.id
  http_method = aws_api_gateway_method.root.http_method
  status_code = aws_api_gateway_method_response.root_response_200.status_code

  # Transforms the backend JSON response to XML
  response_templates = {
    "application/json" = <<-EOF
      #set($inputRoot = $input.path('$'))
      {
        "body": "$inputRoot.body",
        "message": "OK",
        "author": "Barney"
      }
    EOF
  }
}
