variable "api_id" {
  type = string
  description = "Rest API id"
}

variable "root_resource_id" {
  type = string
  description = "Root path resource id"
}