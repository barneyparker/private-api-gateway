resource "aws_api_gateway_rest_api" "gel_api" {
  name = "gel_api"
  description = "Internal GEL API"
  disable_execute_api_endpoint = false # TODO: should be true

  endpoint_configuration {
    types = ["PRIVATE"] # TODO: Once basic functionality works, this should be PRIVATE, otherwise REGIONAL
    vpc_endpoint_ids = [aws_vpc_endpoint.api_endpoint.id]
  }
}

locals {
  route_hashes = [for src in fileset(path.module, "routes/**") : filesha256(src)]

  single_hash = sha256(join(":", local.route_hashes))
}

resource "aws_api_gateway_deployment" "gel_api" {
  rest_api_id = aws_api_gateway_rest_api.gel_api.id

  triggers = {
    redeployment = local.single_hash
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [
    module.routes,
    aws_api_gateway_rest_api_policy.gel_api
  ]
}

resource "aws_api_gateway_stage" "gel_api" {
  deployment_id = aws_api_gateway_deployment.gel_api.id
  rest_api_id   = aws_api_gateway_rest_api.gel_api.id
  stage_name    = "gel_api"
}

module "routes" {
  source = "./routes"

  api_id = aws_api_gateway_rest_api.gel_api.id
  root_resource_id = aws_api_gateway_rest_api.gel_api.root_resource_id
}

resource "aws_api_gateway_rest_api_policy" "gel_api" {
  rest_api_id = aws_api_gateway_rest_api.gel_api.id
  policy = data.aws_iam_policy_document.gel_api.json
}

data "aws_iam_policy_document" "gel_api" {
  statement {
    principals {
      type = "*"
      identifiers = ["*"]
    }

    actions = ["execute-api:Invoke"]
    resources = [
      "${aws_api_gateway_rest_api.gel_api.execution_arn}/*"
    ]
  }
}