# Providers
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.49"
    }

    archive = {
      source  = "hashicorp/archive"
      version = "~> 2.2.0"
    }
  }
  required_version = "~> 1.0.0"
}

provider "aws" {
  region = "eu-west-2"
}

# Backend

# Data-sources
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}